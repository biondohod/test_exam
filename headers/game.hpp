#pragma once
#include <SFML/Graphics.hpp>

//������ � ������ ���� ���������
const int width = 600;
const int height = 600;
//������ � ������ ������� ����
const int gameWidth = 500;
const int gameHeight = 500;
//���������� �������� � ������
const int size = 4;
//������ ����������
const int quadSize = 120;
//����� ���������� ��������� �� ���������� ����
const int N = size * size;
namespace game
{
	enum class Relocation
	{
		Left = 0,
		Right = 1, 
		Up = 2, 
		Down = 3
	};
	class Quad : public sf::Drawable, public sf::Transformable
	{
	public:
		Quad();
		~Quad();
		void Start();
		bool WinChek();
		void Move(Relocation relocation);
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	private:
		int objects[N];
		//1 �� 16 ������������, ������� ����� ������
		int emptyObject;
		bool win;
		sf::Font font;


	};
}