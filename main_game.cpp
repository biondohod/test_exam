﻿#include <SFML/Graphics.hpp>
//#include <SFML/Audio.hpp>
#include <headers/game.hpp>
#include <iostream>
int main()
{
	sf::RenderWindow window(sf::VideoMode(width, height), "Pyatnashki");
	window.setFramerateLimit(60);
	//подключение шрифта+проверка
	sf::Font font;
	font.loadFromFile("fonts/arial.ttf");
	if (!font.loadFromFile("fonts/arial.ttf"))
	{
		std::cout << "Error! Falied to load arial.ttf" << std::endl;
		return -1;
	}
	// текст с обозначением клавиш
	sf::Text text("R - New Game / Esc - Exit / Arrow Keys - Move Tile", font, 20);
	text.setFillColor(sf::Color::Cyan);
	text.setPosition(5, 5);
	game::Quad game;
	game.setPosition(50, 50);
	int moveCount = 0;
	sf::Event event;
	//sf::SoundBuffer clickBuffer;
	//clickBuffer.loadFromFile("sound/clickSound.wav");
	//sf::Sound click(clickBuffer);
	while (window.isOpen())
	{
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) window.close();
			if (event.type == sf::Event::KeyPressed)
			{
				//при нажатии esc - закрытие программы
				if (event.key.code == sf::Keyboard::Escape) window.close();
				//управление
				if (event.key.code == sf::Keyboard::Left)
				{
					game.Move(game::Relocation::Left);
					//click.play();
				}
				if (event.key.code == sf::Keyboard::Right)
				{
					game.Move(game::Relocation::Right);
					//click.play();
				}
				if (event.key.code == sf::Keyboard::Up)
				{
					game.Move(game::Relocation::Up);
					//click.play();
				}
				if (event.key.code == sf::Keyboard::Down)
				{
					game.Move(game::Relocation::Down);
					//click.play();
				}
				// Новая игра
				if (event.key.code == sf::Keyboard::R)
				{
					game.Start();
					moveCount = 100;
				}
			}
			
		}
		if (moveCount-- > 0) 
			game.Move((game::Relocation)(rand() % 4));
		window.clear();
		window.draw(game);
		window.draw(text);
		window.display();
	}
	return 0;
}