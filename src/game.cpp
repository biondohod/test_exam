#include <headers/game.hpp>
#include <SFML/Graphics.hpp>
//#include <SFML/Audio.hpp>
namespace game
{
	Quad::Quad()
	{
		font.loadFromFile("fonts/arial.ttf");
		Start();
	}
	Quad::~Quad()
	{

	}
	void Quad::Start()
	{
		//����������� �������� ����������� �� 1 �� 15
		for (int i = 0; i<N; i++)
		{
			objects[i] = i+1;
		}
		//������ ������ ������� ���������� ��������� �������� �������
		emptyObject = N - 1;
		objects[emptyObject] = 0;
		//�� ��� ���������� ����������� �� �������, ������� ��� �� ������
		win = true;
	}
	bool Quad::WinChek()
	{
		//��������. ���� ��� ��������(�� ������ �������) ����������� �� �������, �� ����������� ��� ��� ������
		for (int i = 0; i < N; i++)
		{
			if (objects[i] > 0 && objects[i] != i + 1)
				return false;
		}
		return true;
	}
	void Quad::Move(Relocation relocation)
	{
		//���������� ������ � ������� ������� ����������
		int column = emptyObject % size;
		int string = emptyObject / size;
		//�������� �� ��, �������� �� ����������� � ����������� ������� ������
		//��� ��� �� ������� �� ������ ���������, � ����������� �� ��� �����, �� ��������� ��� ����� ��������
		//�� ����, ���� �� ����� �������� ��� ��������� �����, �� ������ ������ ������ �� � ������� ���� � ��
		int moveIndex = -1;
		if (relocation == Relocation::Left && column < (size - 1))
			moveIndex = emptyObject + 1;
		if (relocation == Relocation::Right && column > 0)
			moveIndex = emptyObject - 1;
		if (relocation == Relocation::Up && string < (size - 1))
			moveIndex = emptyObject + size;
		if (relocation == Relocation::Down && string > 0)
			moveIndex = emptyObject - size;
		if (emptyObject >= 0 && moveIndex >= 0)
		{
			int tmp = objects[emptyObject];
			objects[emptyObject] = objects[moveIndex];
			objects[moveIndex] = tmp;
			emptyObject = moveIndex;
		}
			//std::swap(objects[emptyObject], objects[moveIndex]);
		//�������� �� ������ ����� ������������ ����������� ����������
		win = WinChek();
		
	}
	void Quad::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		states.transform *= getTransform();
		sf::Color color = sf::Color(sf::Color::Magenta);
		// ������ ����� �������� ����
		sf::RectangleShape shape(sf::Vector2f(gameWidth, gameHeight));
		shape.setOutlineThickness(2);
		shape.setOutlineColor(color);
		shape.setFillColor(sf::Color::Transparent);
		target.draw(shape, states);
		//������ �����, � ������� ��������� ��� ����������
		shape.setSize(sf::Vector2f(quadSize - 2, quadSize - 2));
		shape.setOutlineThickness(2);
		shape.setOutlineColor(color);
		shape.setFillColor(sf::Color::Transparent);
		//�����
		sf::Text text("text", font, 52);
		for (int i = 0; i < N; i++)
		{
			shape.setOutlineColor(color);
			text.setFillColor(color);
			text.setString(std::to_string(objects[i]));
			if (win)
			{
				// ��� ������ �������� ��� ������ ������
				shape.setOutlineColor(sf::Color::Cyan);
				text.setFillColor(sf::Color::Cyan);
			}
			else if (objects[i] == i+1)
			{
				//����������, ������� ��������� �� ����� ������ ������ �������
				text.setFillColor(sf::Color::Green);
			}
			//��������� ���� �����������, ����� �������
			if (objects[i] > 0)
			{
				//���������� ������� ���������� ��� ���������
					sf::Vector2f position(i % size * quadSize + 10, i / size * quadSize + 10);
				shape.setPosition(position);
				// ������� ������
				text.setPosition(position.x + 30.f + (objects[i] < 10 ? 15 : 0), position.y + 25);
				// ������������� ����� ����������
				target.draw(shape, states);
				// ������������ �������� ����������
				target.draw(text, states);
			}
		}
	}
	
}